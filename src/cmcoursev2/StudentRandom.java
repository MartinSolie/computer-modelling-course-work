package cmcoursev2;

import java.util.Random;
import umontreal.iro.lecuyer.probdist.StudentDist;

/**
 * Class for generating random value with probability density equal to Student's
 * T-distribution with specified freedom degree
 * @author martin
 */
public class StudentRandom {
    
    private StudentDist stDist;
    private Random gamma;
    private double a, b;
    private double metropolisSeed;
    private double metropolisDelta;
    private double max;
    
    /**
     * Default constructor sets
     * freedom degree value to 1
     * lower bound (a) to -9
     * upper bound (b) to 9
     * <p>
     * Calls setUpMetropolis() method
     * 
     * @see setUpMetropolis()
     */    
    public StudentRandom(){
        stDist = new StudentDist(1);        
        gamma = new Random();        
        a = -9;
        b = 9;
        max = 0.45;
        setUpMetropolis();
    }
    
    /**
     * Sets freedom degree of student distribution to freedomDegree
     * lower and upper bounds of generator to a and b
     * calls setUpMetropolis()
     * @see setUpMetropolis()
     * @param freedomDegree
     * @param a
     * @param b 
     */
    public StudentRandom(int freedomDegree, double a, double b){        
        stDist = new StudentDist(freedomDegree);        
        gamma = new Random();        
        
        setBorders(a,b);        
        if (a * b < 0 || getStudentDistribution().density(b) > 0.01 || getStudentDistribution().density(a) > 0.01){
            setUpMetropolis();
        }        
        
        if (a * b < 0){
            max = 0.45;
        } else if (stDist.density(b) > stDist.density(a)){
            max = stDist.density(b);
        } else {
            max = stDist.density(a);
        }
    }   
    
    // =============================== Accessors
    public double getA(){
        return a;
    }
    
    public double getB(){
        return b;
    }
    
    public final StudentDist getStudentDistribution(){
        return stDist;
    }
    // =========================================
    
    /**
     * Set lower bound to a
     * upper bound to b
     * Checks the fact that a < b
     * Calls setUpMetropolis()
     * @see setUpMetropolis()
     * @param a
     * @param b 
     */    
    final public void setBorders (double a, double b){
        if (a < b){
            this.a = a;
            this.b = b;            
        } else if (a > b) {
            this.a = b;
            this.b = a;            
        } else {
            this.a = 0;
            this.b = 1;            
            System.out.println("a cannot be equal to b");            
        }
    }
    
    /**
     * Run metropolis method for 100 times, to get really random value, when user will
     * call nextMetropolisDouble();
     * @see nextMetropolisDouble();
     */
    private void setUpMetropolis(){
        metropolisSeed = (b-a)/2;           //start value of wandering
        metropolisDelta = (double)(b-a)/4;  //wandering step        
        
        for (int i = 0; i<100; i++){
            nextMetropolisDouble();
        }
    }
    
    /**
     * Returns random value, having probability density as probability density
     * of Student's T-distribution, using standard generator and Neumann's method
     * @return random value
     */    
    public double nextNeumannDouble(){
        double x, y;
        do {
            x = a + (b - a) * gamma.nextDouble();
            y = max * gamma.nextDouble();            
        } while (stDist.density(x) < y);
        return x;
    }
    
    /**
     * Returns random value, having probability density as probability density
     * of Student's T-distribution, using standard generator and method of reverse
     * function
     * @return random value
     */
    public double nextReverseDouble (){        
        return stDist.inverseF(stDist.cdf(a) + (stDist.cdf(b)-stDist.cdf(a))*gamma.nextDouble());                
    }
    
    /**
     * Returns random value, having probability density as probability density
     * of Student's T-distribution, using standard generator and Metropolis method 
     * @return random value
     */
    public double nextMetropolisDouble(){
        do {
            double nextSeed = metropolisSeed + (-1 + 2 * gamma.nextDouble())*metropolisDelta;    

            if (stDist.density(nextSeed) > stDist.density(metropolisSeed)
                   || gamma.nextDouble() < stDist.density(nextSeed) / stDist.density(metropolisSeed)) {

                metropolisSeed = nextSeed;            
            } 
        } while (metropolisSeed < a || metropolisSeed > b);
        
        return metropolisSeed;
    }
}
