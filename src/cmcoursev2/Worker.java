package cmcoursev2;

import expdata.OutputData;
import gui.ResultForm;
import gui.WorkerForm;
import java.awt.event.WindowEvent;

/**
 * Class for conducting experiments for 3 methods of generating random number
 * with density of Student's T-distribution
 * @author martin
 */
public class Worker extends Thread {
    private StudentRandom stRandom;
    private long N;
    private double histDelta;    
    private WorkerForm form;
    
    private OutputData outData;
    
    private volatile boolean aborted = false;
    private volatile boolean paused = false;
    
    public Worker (StudentRandom stRandom, long N, double histDelta, WorkerForm form){
        this.stRandom = stRandom;
        this.N = N;
        this.histDelta = histDelta;
        this.form = form;
    }
    
    @Override
    public void run(){        
        
        
        
        //length of the interval
        int length = (int)(stRandom.getB() - stRandom.getA() + 1);  
        
        //array for storing histogramm data    
        outData = new OutputData((int)(length/histDelta)+1);        
        
        //for storing generated random value
        double x;
        
        //for balansing array's index (lower bound of interval could be negative,
        //and lower bound of array - no)
        double k = -stRandom.getA();//(stRandom.getA() > 0) ? 0 : -stRandom.getA();        
        
        double progressBarStep = N / 100;
        
        //=============================Experiment===============================        
        
        int i = 0;
        
        if (!aborted){
            for (i = 0; i<N; i++){

                if (stopExecution()) { break; }         //checks whether process was paused or aborted
                
                //--------------------Reverse function method-------------------
                x = stRandom.nextReverseDouble();               
                outData.reverseAvg += x;                        
                outData.reverseD2 += x*x;
                outData.reverseData[(int)((x+k)/histDelta)]++;           
                //--------------------------------------------------------------
                
                //--------------------Neumann's method--------------------------
                x = stRandom.nextNeumannDouble();
                outData.neumannAvg += x;
                outData.neumannD2 += x*x;
                outData.neumannData[(int)((x+k)/histDelta)]++;
                //--------------------------------------------------------------
                
                //--------------------Metropolis method-------------------------
                x = stRandom.nextMetropolisDouble();
                outData.metropolisAvg += x;
                outData.metropolisD2 += x*x;
                outData.metropolisData[(int)((x+k)/histDelta)]++;
                //--------------------------------------------------------------
                
                //if 1% of computations passed - move progress bar
                if (i % progressBarStep == 0) {
                    form.performProgressBarStep();
                }
            }         
        }
        //======================================================================
        
        if (i == 0) return;

        double normK = stRandom.getStudentDistribution().cdf(stRandom.getB()) - stRandom.getStudentDistribution().cdf(stRandom.getA());
        
        //normalizating histogramms
        for (int j = 0; j<outData.reverseData.length; j++){
            outData.reverseData[j] = outData.reverseData[j] / i / histDelta * normK;           
            outData.neumannData[j] = outData.neumannData[j] / i / histDelta * normK;
            outData.metropolisData[j] = outData.metropolisData[j]/ i / histDelta * normK;           
        }
        
        // Computing means, variances and sigmas
        outData.reverseAvg /= i;
        outData.reverseD2 = Math.abs((outData.reverseD2 - i * outData.reverseAvg * outData.reverseAvg)
                / (i-1));        
        outData.reverseSigma = Math.sqrt(outData.reverseD2 / i);
        
        outData.neumannAvg /= i;
        outData.neumannD2 = Math.abs((outData.neumannD2 - i * outData.neumannAvg * outData.neumannAvg)
                / (i - 1));
        outData.neumannSigma = Math.sqrt(Math.abs(outData.neumannD2) / i);
        
        outData.metropolisAvg /= i;
        outData.metropolisD2 = Math.abs((outData.metropolisD2 - i * outData.metropolisAvg * outData.metropolisAvg)
                / (i - 1));
        outData.metropolisSigma = Math.sqrt(outData.metropolisD2/i);        
        
        //saving initial data
        outData.Nst = i;
        outData.freedomDegree = (int)stRandom.getStudentDistribution().getParams()[0];  
        outData.a = stRandom.getA();
        outData.b = stRandom.getB();
        
        //fills up array of coordinates with histDelta step        
        outData.coordinates[0] = stRandom.getA();
        for(i = 1; i<outData.coordinates.length; i++){
            outData.coordinates[i] = outData.coordinates[i-1]+histDelta;
        }
        
        //fills up array for building density plot with histDelta step        
        for (i = 0; i < outData.coordinates.length; i++){
            outData.densityPlotData[i] = stRandom.getStudentDistribution().density(outData.coordinates[i]);
        }
        
        //closes worker window
        form.dispatchEvent(new WindowEvent(form, WindowEvent.WINDOW_CLOSING));
        
        //shows result window
        ResultForm resultForm = new ResultForm (outData);
        resultForm.setVisible(true);        
    }
    
    //methods for aborting and pausing worker thread
    private boolean stopExecution(){
        if (aborted) return true;
            
        while (paused) {
            try {
                Thread.sleep( 1000 );
                if (aborted) return true;
            } catch (InterruptedException ex) {
                System.out.println("InterruptException");
            }
        }
        
        return false;
    }
    
    public void abort(){
        aborted = true;
    }
    
    public void pause(){
        paused = true;
    }
    
    public void unpause(){
        paused = false;
    }
    
    public boolean isPaused(){
        return paused;
    }    
}
