package cmcoursev2;

import gui.MainForm;

/**
 *
 * @author martin
 */
public class Main {

    /**
     * @param args the command line arguments
     */  
    
    public static void main(String[] args) {     
        
        MainForm form = new MainForm();
        form.setVisible(true);
        
    }
    
}
