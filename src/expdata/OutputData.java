/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expdata;

import java.io.Serializable;

/**
 *
 * @author martin
 */
public class OutputData implements Serializable{
    
    //================Initital values================
    public int freedomDegree = 1;
    public double a = 0;
    public double b = 0;
    public long Nst = 0;
    //===============================================
        
    //================Integral values================    
    public double analyticsD2   =   0;
    
    public double reverseAvg    =   0;
    public double reverseD2     =   0;
    public double reverseSigma  =   0;
    
    public double neumannAvg    =   0;
    public double neumannD2     =   0;
    public double neumannSigma  =   0;
    
    public double metropolisAvg =   0;
    public double metropolisD2  =   0;
    public double metropolisSigma = 0; 
    //===============================================
    
    //==================Plot data====================
    public double[] reverseData;
    public double[] neumannData;
    public double[] metropolisData;
        
    public double[] coordinates;
    public double[] densityPlotData;
    //===============================================
    
    public OutputData(){}
    
    public OutputData (int size){
        reverseData = new double[size];
        neumannData = new double[size];
        metropolisData = new double[size];
        coordinates = new double[size];
        densityPlotData = new double[size];        
    }
    
}
