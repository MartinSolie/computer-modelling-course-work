package gui;

import expdata.OutputData;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import org.math.plot.Plot2DPanel;

/**
 *
 * @author martin
 */
public class ResultForm extends JFrame {
    
    private static int experimentNumber = 1;   
    
    private final Plot2DPanel probabilityDensityPlot;    
    private final JTextArea initialValuesText;
    private final JTable integralTable;
    private final JButton saveButton;
    private final JFileChooser fc = new JFileChooser();
    
    private OutputData outData;
    
    public ResultForm (OutputData outData){
        
        this.outData = outData;
        
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        setSize(800, 600);
        setTitle(experimentNumber++ + " experiment results");
        setIconImage(new ImageIcon(getClass().getResource("/resources/icon.png")).getImage());
        setLayout(new BorderLayout());
        
        //==============================================Initial values table================================================
        String initValuesString = "\n\n\nInitials:\n"
                                + "Freedom:\t" + outData.freedomDegree + "\n"
                                + "Nst:\t" + outData.Nst + "\n"
                                + "a:\t" + outData.a + "\n"
                                + "b:\t" + outData.b;        
        initialValuesText = new JTextArea (initValuesString);
        initialValuesText.setPreferredSize(new Dimension (170,450));
        initialValuesText.setEditable(false);
        add (initialValuesText, BorderLayout.WEST);
        //==================================================================================================================
        
        //=============================================Integral values table================================================
        String[] columnNames = {"Method", "Mean", "Variance", "\u03C3"};
        Object[][] data = {
            {"Theoretical", 0, outData.freedomDegree > 2 ? (double)outData.freedomDegree / (outData.freedomDegree - 2) : 0, 0},
            {"Reverse", outData.reverseAvg, outData.reverseD2, outData.reverseSigma},
            {"Neumann", outData.neumannAvg, outData.neumannD2, outData.neumannSigma},
            {"Metropolis", outData.metropolisAvg, outData.metropolisD2, outData.metropolisSigma}
        };
        
        integralTable = new JTable(data, columnNames);
        integralTable.setEnabled(false);
                        
        JPanel tablePanel = new JPanel();
        tablePanel.setLayout(new GridLayout(2,1));        
        tablePanel.add(integralTable.getTableHeader());
        tablePanel.add(integralTable);
        add(tablePanel, BorderLayout.NORTH);       
        //==================================================================================================================
        
        //================================================Creating plot=====================================================
        probabilityDensityPlot = new Plot2DPanel();       
        
        probabilityDensityPlot.addLinePlot("Probability density", Color.BLACK, outData.coordinates, outData.densityPlotData);
        probabilityDensityPlot.addStaircasePlot("Reverse method", Color.RED, outData.coordinates, outData.reverseData);
        probabilityDensityPlot.addStaircasePlot("Neumann's method", Color.GREEN, outData.coordinates, outData.neumannData);
        probabilityDensityPlot.addStaircasePlot("Metropolis method", Color.BLUE, outData.coordinates, outData.metropolisData);        
        probabilityDensityPlot.setFixedBounds(0, outData.a, outData.b);
        probabilityDensityPlot.addLegend("south");        
        add(probabilityDensityPlot, BorderLayout.CENTER);
        //==================================================================================================================
        
        
        saveButton = new JButton("Save results");
        saveButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                saveData();
            }
        
        });
        add(saveButton, BorderLayout.SOUTH);
        
        setLocationRelativeTo(null);
        
        fc.setCurrentDirectory(new File("./"));
    }   
    
    public void saveData(){
        String filepath;
        
        int returnVal = fc.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            filepath = file.getAbsoluteFile().toString();            
            filepath += ".sav";
        } else {
            return;
        }        
        
        FileOutputStream fos = null;
        try {
            
            fos = new FileOutputStream(filepath);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(outData);
            oos.flush(); 
            oos.close();
            
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "No such directory", "Directory error", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Unexpected IO exception try later, please.", "IO error", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(ResultForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        JOptionPane.showMessageDialog(null, "File saved succesfully.", "OK", JOptionPane.INFORMATION_MESSAGE);
    }
    
    /*
    public void addHistogram (String title, double[] data){          
        probabilityDensityPlot.addStaircasePlot(title, coordinates, data);        
    } 
    */
}
