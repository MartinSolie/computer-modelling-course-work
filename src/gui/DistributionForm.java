/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 *
 * @author martin
 */
public class DistributionForm extends JFrame {
    
    private static DistributionForm form = null;
    
    public static void showForm(){
        if (form == null){
            form = new DistributionForm();
            form.setVisible(true);
        } else {
            form.setVisible(true);
            form.toFront();
        }
    }
    
    private JEditorPane editorPane = new JEditorPane();
    
    public DistributionForm(){
        
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        
        setTitle("About: Student\'s distribution");
        setIconImage(new ImageIcon(getClass().getResource("/resources/icon.png")).getImage());
        editorPane.setEditable(false);        
        java.net.URL helpURL = getClass().getResource("/resources/method.html");
        
        if (helpURL != null){
            try{
                editorPane.setPage(helpURL);
            } catch (IOException e){
                JOptionPane.showMessageDialog(null, "Try to access bad resource file.\nApplication file seems to be broken.", "Resource error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            System.out.println("Could not find Resource file");
        }
        
        JScrollPane scrollPane = new JScrollPane(editorPane);
        
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
//        scrollPane.setMaximumSize(new Dimension(1920,1080));
//        scrollPane.setMinimumSize(new Dimension(350,100));
        scrollPane.setPreferredSize(new Dimension(720, 350));
       
        
        add(scrollPane, BorderLayout.CENTER);
        
        pack();
        
        setLocationRelativeTo(null);
    }
}
