/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 *
 * @author martin
 */
public class InstructionsForm extends JFrame{
    
    private static InstructionsForm form = null;
    
    public static void showForm(){
        if (form == null ){
            form = new InstructionsForm();
            form.setVisible(true);
        } else {
            form.setVisible(true);
            form.toFront();
        }
    }
    
    private JEditorPane editorPane = new JEditorPane();
    
    private InstructionsForm (){
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        
        setTitle("Help");
        setIconImage(new ImageIcon(getClass().getResource("/resources/icon.png")).getImage());
        editorPane.setEditable(false);
        
        java.net.URL helpURL = getClass().getResource("/resources/instructions.html");
        
        if (helpURL != null){
            try{
                editorPane.setPage(helpURL);                
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Try to access bad resource file.\nApplication file seems to be broken.", "Resource error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Try to access bad resource file.\nApplication file seems to be broken.", "Resource error", JOptionPane.ERROR_MESSAGE);
        }
        
        JScrollPane scrollPane = new JScrollPane(editorPane);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setPreferredSize(new Dimension(720, 350));
        add(scrollPane);
        pack();
        setLocationRelativeTo(null);
        
    }
}
