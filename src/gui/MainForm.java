/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import cmcoursev2.StudentRandom;
import expdata.OutputData;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTextField;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;




/**
 *
 * @author martin
 */
public class MainForm extends JFrame{
    //private double[] histogrammData;    
    
    private JMenuBar menuBar;
    private JMenu menuAbout, menuHelp;
    private JMenuItem menuItem;
    
    
    private final JTextField aText = new JTextField("-9");
    private final JTextField bText = new JTextField("9");
    private final JTextField freedomDegreeText = new JTextField("4");
    private final JTextField nText = new JTextField("100000");
    private final JTextField hColumnText = new JTextField("50");
    private final JFileChooser fc = new JFileChooser();
    private JButton okButton;
    private JButton exitButton;
    
    public MainForm (){
                
        this.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent ev){
                System.exit(0);
            }
        });
        
        setTitle("Studnet\'s T-distribution generator");
        setSize (400,300);
        setIconImage(new ImageIcon(getClass().getResource("/resources/icon.png")).getImage());
        setLayout(new GridLayout(6,2));
        
        menuBar = new JMenuBar();
        menuAbout = new JMenu("About");
        menuHelp = new JMenu("Help");
        menuBar.add(menuAbout);
        menuBar.add(menuHelp);
        
        menuItem =  new JMenuItem("Student\'s T-Distribution");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                DistributionForm.showForm();
            }
        });
        menuAbout.add(menuItem);
        
        menuItem = new JMenuItem("Author");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                AuthorForm.showForm();
            }
        });
        menuAbout.add(menuItem);
        
        menuItem = new JMenuItem("Guidelines");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                InstructionsForm.showForm();
            }
        });
        menuHelp.add(menuItem);
        
        setJMenuBar(menuBar);
        
        add(new JLabel("freedom degree:"));
        add(freedomDegreeText);
        add(new JLabel("a:"));
        add(aText);
        add(new JLabel("b:"));
        add(bText);        
        add(new JLabel("Nst:"));
        add(nText);
        add(new JLabel("histogram columns amount:"));
        add(hColumnText);
        
        okButton = new JButton("Start");
        okButton.addActionListener (new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                startComputation();                
            }       
        });
        add(okButton);
        
        exitButton = new JButton("Load");
        exitButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                //exitButtonClicked();
                loadData();
            }        
        });
        add(exitButton);
        
        setResizable(false);
        setLocationRelativeTo(null);
        
        fc.setCurrentDirectory(new File("./"));        
    }
    
    private void startComputation(){
        int freedomDegree;
        double a, b;
        int hColumn;
        long Nst;
        try{
            a = Double.parseDouble(aText.getText());
            b = Double.parseDouble(bText.getText());
            hColumn = Integer.parseInt(hColumnText.getText());
            
            freedomDegree = Integer.parseInt(freedomDegreeText.getText());
            Nst = Long.parseLong(nText.getText());
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this,
                "Input correct data, please.\n"
                + "Only numbers.\n"
                + "a, b are floating point numbers.\n"
                + "Nst, amount of histogram columns and freedomDegree are positive integeres.", 
                "Number format exception", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if( a > b){
            JOptionPane.showMessageDialog(this, " Start of the interval (a)\n"
                    + " should be less than end of the interval (b).\n Swapped.",
                    "Interval warning", JOptionPane.WARNING_MESSAGE);
            double tmp = a;
            a = b;
            b = tmp;
        } else if (a == b) {
            JOptionPane.showMessageDialog(this, "Start (a) cannot be equal to end (b).\n Aborting.",
                    "Interval warning", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if (hColumn < 1){
            JOptionPane.showMessageDialog(this, "Histogram should have at least 1 column\n Aborting.",
                    "Histogram step size mismatch", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if (freedomDegree < 1 || Nst < 0){
            JOptionPane.showMessageDialog(this, "Freedom degree should be larger than 1.\nNst is non negative only.\nAborting.",
                    "Negative value.", JOptionPane.ERROR_MESSAGE);
            return;
        }
                
        StudentRandom stRand = new StudentRandom(freedomDegree, a, b);                
        
        if (stRand.getStudentDistribution().cdf(b) - stRand.getStudentDistribution().cdf(a) < 0.01){
            JOptionPane.showMessageDialog(null, "Density of distribution on the given interval is too small.\nAborting.", "To small density", JOptionPane.ERROR_MESSAGE);
            return;
        }        
       
        WorkerForm wForm = new WorkerForm (stRand, Nst, (b - a) / hColumn);
        wForm.setVisible(true);       
    }
    
    private void loadData(){
        String absoluteFilename = "";
        
        int returnVal = fc.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();            
            
            absoluteFilename = file.getAbsoluteFile().toString();
        } else {
            return;
        }        
        
        FileInputStream fis = null;
        
        try {
            
            fis = new FileInputStream(absoluteFilename);
            ObjectInputStream oin = new ObjectInputStream(fis);
            OutputData outData = (OutputData) oin.readObject();
            
            ResultForm resForm = new ResultForm(outData);
            resForm.setVisible(true);
            
        } catch (FileNotFoundException ex) {
            
            JOptionPane.showMessageDialog(null, "No such file", "File error", JOptionPane.ERROR_MESSAGE);
            
        } catch (IOException ex) {
            
            JOptionPane.showMessageDialog(null, "Unexpected IO exception try later, please.", "IO error", JOptionPane.ERROR_MESSAGE);
            
        } catch (ClassNotFoundException ex) {
            
            JOptionPane.showMessageDialog(null, "Seriously, this is unreal, if you see this error - reinstall the application, pleas.", "Class not found error", JOptionPane.ERROR_MESSAGE);
            
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Error closing filestream.", "IO error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    private void exitButtonClicked(){
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }
    
}
