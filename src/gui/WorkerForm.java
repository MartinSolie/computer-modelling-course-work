/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import cmcoursev2.StudentRandom;
import cmcoursev2.Worker;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author martin
 */
public class WorkerForm extends JFrame {
    private Worker worker;
    private JProgressBar pb;
    private JButton pauseButton;
    private JButton stopButton;
    
    public WorkerForm (StudentRandom stRand, long Nst, double histDelta){
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        
        
        setTitle ("Computing ...");    
        setIconImage(new ImageIcon(getClass().getResource("/resources/icon.png")).getImage());
        setLayout(new GridBagLayout());
        
        pb = new JProgressBar(0, 100);
        pb.setPreferredSize(new Dimension(300, 20));        
        
        pauseButton = new JButton ("Pause");
        pauseButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                pauseButtonClicked();
            }            
        });
        
        stopButton = new JButton ("Stop");
        stopButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent ae) {
                stopButtonClicked();                
            }
        
        });
        
        GridBagConstraints gbc = new GridBagConstraints();        
        
        //==================== adding progress bar
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 10, 10, 10);
        gbc.gridwidth = 3;        
        gbc.gridy = 0;
        gbc.gridx = 0;        
        
        add (pb, gbc);
        
        //===================== adding pause button
        gbc.anchor = GridBagConstraints.EAST;
        gbc.fill = GridBagConstraints.NONE;        
        gbc.insets = new Insets(10, 2, 10, 2);        
        gbc.gridwidth = 1;        
        gbc.weightx = 2;
        gbc.ipadx = 25;
        gbc.gridy = 1;                
        gbc.gridx = 1;
        
        add (pauseButton, gbc);
                
        //====================== adding stop button
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 2, 10, 10);
        gbc.weightx = 1;
        gbc.ipadx = 0;        
        gbc.gridx = 2;
        
        add (stopButton, gbc);        
        
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        
        WindowOpenedListener wol = new WindowOpenedListener(stRand, Nst, histDelta, this);
        this.addWindowListener(wol);
    }
    
    public void performProgressBarStep(){
        this.pb.setValue(this.pb.getValue()+1);
    }
    
    private void pauseButtonClicked(){
        if (worker.isPaused()){
            worker.unpause();
            this.pauseButton.setText("Pause");
        } else {
            worker.pause();
            this.pauseButton.setText("Continue");
        }
    }
    
    private void stopButtonClicked(){
        worker.abort();
        this.setVisible(false);
    }
    
    private class WindowOpenedListener extends WindowAdapter{
        protected StudentRandom stRand;
        protected long Nst;
        protected double histDelta;
        protected WorkerForm form;
        
        public WindowOpenedListener(StudentRandom stRand, long Nst, double histDelta, WorkerForm form){
            this.stRand = stRand;
            this.Nst = Nst;
            this.histDelta = histDelta;
            this.form = form;
        }        
        
        @Override
        public void windowOpened(WindowEvent ev){
            worker = new Worker(stRand, Nst, histDelta, form);
            worker.start();
        }
    } 
}
