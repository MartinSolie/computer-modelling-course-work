/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author martin
 */
public class AuthorForm extends JFrame{
    
    private static AuthorForm form = null;
    
    public static void showForm(){
        if (form == null){
            form = new AuthorForm();
            form.setVisible(true);
        } else {
            form.setVisible(true);
            form.toFront();
        }
    }
    
    private AuthorForm(){       
        
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        
        setTitle("About: Author");
        setIconImage(new ImageIcon(getClass().getResource("/resources/icon.png")).getImage());
        setLayout(new BorderLayout());
        
        add (new JLabel(new ImageIcon(getClass().getResource("/resources/me.png"))), BorderLayout.CENTER);
        
        JPanel p = new JPanel();
        p.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        
        gbc.gridx = 0;
        gbc.gridy = 0;
        p.add (new JLabel("Anthony Martynov"), gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 1;        
        p.add (new JLabel("M_art_in995@yahoo.com"), gbc);        
        
        add (p, BorderLayout.EAST);        
        
        pack();
        setResizable(false);
        setLocationRelativeTo(null);        
    }
}
